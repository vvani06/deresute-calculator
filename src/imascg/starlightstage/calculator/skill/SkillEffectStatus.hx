package imascg.starlightstage.calculator.skill;

enum SkillEffectStatus {
  Normal;
  Boosted;
}
