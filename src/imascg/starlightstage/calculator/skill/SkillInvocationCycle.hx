package imascg.starlightstage.calculator.skill;

class SkillInvocationCycle {
  var seconds: Int;
  private function new(seconds) {
    this.seconds = seconds;
  }
  public static var Seconds04 (default, null) = new SkillInvocationCycle(4);
  public static var Seconds05 (default, null) = new SkillInvocationCycle(5);
  public static var Seconds06 (default, null) = new SkillInvocationCycle(6);
  public static var Seconds07 (default, null) = new SkillInvocationCycle(7);
  public static var Seconds08 (default, null) = new SkillInvocationCycle(8);
  public static var Seconds09 (default, null) = new SkillInvocationCycle(9);
  public static var Seconds10 (default, null) = new SkillInvocationCycle(10);
  public static var Seconds11 (default, null) = new SkillInvocationCycle(11);
  public static var Seconds12 (default, null) = new SkillInvocationCycle(12);
  public static var Seconds13 (default, null) = new SkillInvocationCycle(13);
  public static var Seconds14 (default, null) = new SkillInvocationCycle(14);
  public static var Seconds15 (default, null) = new SkillInvocationCycle(15);
  public static var Seconds17 (default, null) = new SkillInvocationCycle(17);
  public static var Seconds18 (default, null) = new SkillInvocationCycle(18);
}
