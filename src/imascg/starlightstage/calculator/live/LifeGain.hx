package imascg.starlightstage.calculator.live;

class LifeGain {
  public var value(default, null): Int;
  public function new(value) {
    this.value = value;
  }
}
