package imascg.starlightstage.calculator.live;

enum RhythmIconJudgement {
  Perfect;
  Great;
  Nice;
  Bad;
  Miss;
}
