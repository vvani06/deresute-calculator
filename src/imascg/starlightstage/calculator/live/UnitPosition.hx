package imascg.starlightstage.calculator.live;

enum UnitPosition {
  Center;
  CenterRight;
  CenterLeft;
  Right;
  Left;
}
