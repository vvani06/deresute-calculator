package imascg.starlightstage.calculator.idol;

enum TrainingStatus {
  Untrained;
  Trained;
}
