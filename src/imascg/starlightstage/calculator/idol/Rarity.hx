package imascg.starlightstage.calculator.idol;

enum Rarity {
  Normal(trainingStatus: TrainingStatus);
  Rare(trainingStatus: TrainingStatus);
  SRare(trainingStatus: TrainingStatus);
  SSRare(trainingStatus: TrainingStatus);
}
